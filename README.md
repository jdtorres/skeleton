# Project skeleton
This is a skeleton for research projects in the [Quantum Tinkerer](https://quantumtinkerer.tudelft.nl/)
group at TU Delft. It sets out a common directory structure between projects, and automatically
configures things like continuous integration.

Anyone is free to use this skeleton (thanks to its CC0 license!), however if you are not
a member of the QT group at TU Delft, your mileage may vary (at the very least you will
need to tweak the setup script).

## Creating a new project
Here's how you use this skeleton:

    git clone https://gitlab.kwant-project.org/qt/skeleton my-new-project
    cd my-new-project
    ./setup

![usage](https://gitlab.kwant-project.org/qt/skeleton/uploads/ace628dcd07163fdfb14922fcdcae515/Peek_2017-10-20_13-43.webm)

Running `./setup` requires the `gitpython` and `python-gitlab` Python packages
(these are installed on `io`) and will also ask you for your Gitlab private token,
which you can get from https://gitlab.kwant-project.org/-/profile/personal_access_tokens.

## Working on a project
The project is split into directories that are for holding different types of work.
Use soft links (`ln -s`) if you need to refer to files in a different directory

### `codes`
A Python package containing generic code used in the project. This is the place to
put things like helper functions for defining Kwant systems.

### `data`
A git submodule where all data should be kept.

### `analysis`
A collection of notebooks and python modules for running simulations and data analysis.
This is the place where "daily" work is done. Contains soft links to `codes` (so that scripts
and notebooks in this directory may directly `import` modules from `codes`) and `data`
(so that results may be saved).

### `notes`
Latex and other documents. Any `tex` files in this directory are built into PDFs by CI.
Any `svg` files in an `images` subdirectory are rasterized to `png` by CI.

### `publication`
Any publications resulting from the project. Any `tex` files in this directory are built into PDFs by CI.
Any `svg` files in an `images` subdirectory are rasterized to `png` by CI.
