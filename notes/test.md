---
execute: True
---

# Test

This file is used to display the notes of a project, test basic features by executing it in the CI, etc.

```python
import numpy as np
import matplotlib.pyplot as plt
```

## Some math or code

This is some example code

```python
x = np.linspace(-1, 1, 100)
y = x**2 - 6 * x**3

plt.plot(x, y)
```